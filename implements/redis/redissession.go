package redis

import (
	"encoding/json"
	"errors"
	"github.com/go-redis/redis"
	"gitlab.com/Spouk/session"
	"log"
	"net/http"
	"time"
)

type RedisSessionOptions struct {
	RedisOpt       redis.Options
	CookieName     string
	CookieDomain   string
	CookiePath     string
	CookieExpired  time.Duration
	GenerateCookFu func() string
	UseCook        bool
	UseLoad        bool
	UseSave        bool
	ExtLogger      *log.Logger
	Debug          bool
}

type RedisSession struct {
	log *log.Logger
	c   *redis.Client
	Opt RedisSessionOptions
}

func NewRedisSession(opt RedisSessionOptions) *RedisSession {
	rs := &RedisSession{
		c:   redis.NewClient(&opt.RedisOpt),
		Opt: opt,
	}
	if opt.ExtLogger != nil {
		rs.log = opt.ExtLogger
	}
	rs.ConnectDB()
	return rs
}

//connect database
func (rs *RedisSession) ConnectDB() {
	c := redis.NewClient(&rs.Opt.RedisOpt)

	if err := c.Ping().Err(); err != nil {
		panic("Unable to connect to redis " + err.Error())
	}
	rs.c = c
}

//GetKey get key
func (rs *RedisSession) getKey(key string, src interface{}) error {
	val, err := rs.c.Get(key).Result()
	if err == redis.Nil || err != nil {
		return err
	}
	err = json.Unmarshal([]byte(val), &src)
	if err != nil {
		return err
	}
	return nil
}

//SetKey set key
func (rs *RedisSession) setKey(key string, value interface{}, expiration time.Duration) error {
	cacheEntry, err := json.Marshal(value)
	if err != nil {
		return err
	}
	err = rs.c.Set(key, cacheEntry, expiration).Err()
	if err != nil {
		return err
	}
	return nil
}

func (rs *RedisSession) Check(r *http.Request) (string, bool) {
	if v, found := r.Cookie(rs.Opt.CookieName); found == nil {
		return v.Value, true
	}
	return "", false
}
func (rs *RedisSession) Load(box *session.Session) (map[string]interface{}, error) {
	var src = make(map[string]interface{})
	err := rs.getKey(box.Cookid, src)
	if err != nil {
		if rs.Opt.Debug {
			rs.log.Println("--error load from redis: ", err)
		}
	}
	return src, nil

}
func (rs *RedisSession) Extract(r *http.Request) (*session.Session, error) {
	box, found := r.Context().Value("session").(*session.Session)
	if !found {
		if rs.Opt.Debug {
			rs.log.Println("--test extract from server")
		}
		return nil, errors.New("not found session box in current context")
	}
	//adding some request information
	box.Add("ra", r.RemoteAddr)
	box.Add("method", r.Method)
	box.Add("ua", r.UserAgent())
	return box, nil
}
func (rs *RedisSession) Save(d *session.Session) error {
	stock := d.GetStock()
	if cont, err := json.Marshal(&stock); err == nil {
		if rs.Opt.Debug {
			log.Println(string(cont))
		}
		err := rs.setKey(d.Cookid, string(cont), rs.Opt.CookieExpired)
		if err != nil {
			if rs.Opt.Debug {
				rs.log.Println("--error load from redis: ", err)
			}
		}
	} else {
		return err
	}
	return nil
}
