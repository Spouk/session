package main

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"os"

	red "github.com/go-redis/redis"
	"gitlab.com/Spouk/gotool/render"
	"gitlab.com/Spouk/session"
	"gitlab.com/Spouk/session/implements/redis"

	"log"
	"net/http"
	"time"
)

type server struct {
	mux    *chi.Mux
	render *render.Render
	data   *session.Session
	c      *redis.RedisSession
	log    *log.Logger
}

func main() {
	s := server{
		log: log.New(os.Stdout, "[servertest]", log.LstdFlags|log.Lshortfile),
		mux: chi.NewMux(),
		data: session.NewSession(session.Options{
			CookieName:     "tester",
			CookieDomain:   "localhost",
			CookiePath:     "/",
			CookieExpired:  time.Now().Add(time.Hour * 24 * 365 * 3),
			GenerateCookFu: nil,
			UseCook:        true,
			UseLoad:        true,
			UseSave:        true,
		}),
		render: render.NewRender("./examples/templates/*.tmpl", true, nil, true),
	}
	s.c = redis.NewRedisSession(redis.RedisSessionOptions{
		RedisOpt: red.Options{
			Addr:      "127.0.0.1:6379",
			Password:  "",
			DB:        0,
			TLSConfig: nil,
		},
		CookieName:     "",
		CookieDomain:   "",
		CookiePath:     "",
		CookieExpired:  0,
		GenerateCookFu: nil,
		UseCook:        true,
		UseLoad:        true,
		UseSave:        true,
		ExtLogger:      s.log,
		Debug:          false,
	})

	s.data.SetStore(s.c)

	s.mux.Use(s.data.Use)
	s.mux.Use(middleware.Logger)
	s.mux.Use(middleware.StripSlashes)
	s.mux.Use(middleware.Recoverer)

	s.mux.Get("/", s.home)

	log.Fatal(http.ListenAndServe(":4000", s.mux))

}
func (s *server) home(w http.ResponseWriter, r *http.Request) {
	ses, err := s.data.Store.Extract(r)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError)+err.Error(), http.StatusInternalServerError)
		return
	}
	ses.Add("yo", "fromsession")
	ses.AddBin("testerbin", s)
	_ = s.render.Render("index.tmpl", ses.GetStockAll(), w)
}
