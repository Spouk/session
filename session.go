//*******************************************************************
// session [x] creator aleksey martynenko aka spouk // 2021
//-------------------------------------------------------------------
//a package that implements a simple session with binding to a cookie
//with the ability to implement an external interface for loading
/// saving / checking session data
//
//*******************************************************************
package session

import (
	"context"
	"crypto/sha1"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"
)

//-------------------------------------------------------------------------------
// adding new type for adding support use random loggers
//-------------------------------------------------------------------------------
type (
	LoggerWriter interface {
		Printf(format string, a ...interface{})
		Println(a ...interface{})
		Fatal(args ...interface{})
	}
)

//*******************************************************************
//an interface that allows you to implement external use of a
//session with databases for storing and loading sessions
//*******************************************************************
type Store interface {
	//find incoming request cookName in cookies slice
	Check(r *http.Request) (string, bool)
	//load saved old session data from database/file etc...
	Load(d *Session) (map[string]interface{}, error)
	//extract session object from current request context for use in handler
	Extract(r *http.Request) (*Session, error)
	//save session data to database/file etc...
	Save(d *Session) error
}

//*******************************************************************
// session structure
//*******************************************************************
type Session struct {
	stock    map[string]interface{}
	stockbin map[interface{}]interface{}
	Cookid   string
	pool     sync.Pool
	opt      *Options
	Store    Store
}
type Aggregate struct {
	S map[string]interface{}
	B map[interface{}]interface{}
}

//*******************************************************************
// config struct for initialize session instance
//****************************************************************
type Options struct {
	store          Store
	CookieName     string
	CookieDomain   string
	CookiePath     string
	CookieExpired  time.Time
	GenerateCookFu func() string
	UseCook        bool
	UseLoad        bool
	UseSave        bool
	Log            LoggerWriter
}

//make and init new Session instance correctly
func NewSession(opt Options) *Session {
	d := &Session{
		stock:    make(map[string]interface{}),
		stockbin: make(map[interface{}]interface{}),
		pool:     sync.Pool{},
		opt:      &opt,
	}
	if opt.GenerateCookFu == nil {
		d.opt.GenerateCookFu = d.generateCookdef
	}
	if opt.store == nil {
		d.Store = d
	}
	d.pool.New = func() interface{} {
		return &Session{
			stock:    make(map[string]interface{}),
			stockbin: make(map[interface{}]interface{}),
			Cookid:   "",
		}
	}
	if opt.Log == nil {
		opt.Log = log.New(os.Stdout, "[session]\t", log.LstdFlags|log.Lshortfile)
	}

	return d
}

//back stock variables
func (d *Session) GetStock() map[string]interface{} {
	return d.stock
}

func (d *Session) GetStockAll() Aggregate {
	return Aggregate{
		S: d.stock,
		B: d.stockbin,
	}
}

//replace default store interface implementation
func (d *Session) SetStore(s Store) {
	d.Store = s
}

//put value to session container for binary in use template render
func (d *Session) AddBin(k interface{}, v interface{}) {
	d.stockbin[k] = v
}

//get value from session container for binary in use template render
func (d *Session) GetBin(k interface{}) interface{} {
	if v, found := d.stockbin[k]; found {
		return v
	}
	return nil
}

//put value to session container
func (d *Session) Add(k string, v interface{}) {
	d.stock[k] = v
}

//get value from session container
func (d *Session) Get(k string) interface{} {
	if v, found := d.stock[k]; found {
		return v
	}
	return nil
}

//middleware for inject session container to current incoming context
func (d *Session) Use(n http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		box := d.pool.New().(*Session)
		if d.opt.UseCook {
			if v, found := d.Store.Check(request); !found {
				box.Cookid = d.opt.GenerateCookFu()
				nc := http.Cookie{
					Name:    d.opt.CookieName,
					Value:   box.Cookid,
					Path:    d.opt.CookiePath,
					Domain:  d.opt.CookieDomain,
					Expires: d.opt.CookieExpired,
				}
				http.SetCookie(writer, &nc)
				http.Redirect(writer, request, "/", http.StatusTemporaryRedirect)
				return
			} else {
				box.Cookid = v
				if d.opt.UseLoad {
					if dat, err := d.Store.Load(box); err != nil {
						d.opt.Log.Println("-- store.load: ", err)
						http.Error(writer, err.Error(), http.StatusInternalServerError)
						return
					} else {
						box.stock = dat
					}
				}
			}
		}
		ctx := context.WithValue(request.Context(), "session", box)
		n.ServeHTTP(writer, request.WithContext(ctx))
		if d.opt.UseSave {
			if err := d.Store.Save(box); err != nil {
				d.opt.Log.Println("-- store.save: ", err)
			}
		}
		d.pool.Put(box)
	})
}

//generate random hash for new cooking value
func (d *Session) generateCookdef() string {
	rand.Seed(time.Now().UnixNano())
	randString := func() string {
		var characterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

		b := make([]rune, 32)
		for i := range b {
			b[i] = characterRunes[rand.Intn(len(characterRunes))]
		}
		return string(b)
	}()
	hash := sha1.New()
	hash.Write([]byte(randString))
	bs := hash.Sum(nil)
	return fmt.Sprintf("%x", bs)
}

//*******************************************************************
// implement Store interface default and examples
//*******************************************************************
//implement default Store:Load
func (d *Session) Load(da *Session) (map[string]interface{}, error) {
	empty := make(map[string]interface{})
	return empty, nil
}

//implement default  Store:Save
func (d *Session) Save(ds *Session) error {
	//for example convert current session object to json and print os.stdout
	if cont, err := json.Marshal(&ds.stock); err == nil {
		d.opt.Log.Println(string(cont))
	} else {
		return err
	}
	return nil
}

//implement default  Store:Extract
func (d *Session) Extract(r *http.Request) (*Session, error) {
	if s, found := r.Context().Value("session").(*Session); found {
		return s, nil
	}
	return nil, errors.New("session object not found int current context")
}

//implement default  Store:Check
func (d *Session) Check(r *http.Request) (string, bool) {
	if v, found := r.Cookie(d.opt.CookieName); found == nil {
		return v.Value, true
	}
	return "", false
}
