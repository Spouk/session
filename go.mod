module gitlab.com/Spouk/session

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/onsi/gomega v1.16.0 // indirect
	gitlab.com/Spouk/gotool v0.0.0-20210814190327-15c61b74cc1e
)
